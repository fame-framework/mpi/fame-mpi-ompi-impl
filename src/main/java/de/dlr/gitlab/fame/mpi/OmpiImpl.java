package de.dlr.gitlab.fame.mpi;

import java.nio.ByteBuffer;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.logging.Logging;
import mpi.MPI;
import mpi.MPIException;
import mpi.Status;

/** Implements {@link MpiFacade} via OpenMPI parallelisation
 * 
 * @author Christoph Schimeczek */
public class OmpiImpl implements MpiFacade {
	static final String OPEN_MPI_ERROR = "Error from OpenMPI: ";

	private static final Logger LOGGER = LoggerFactory.getLogger(OmpiImpl.class);
	private int rank;
	private int size;

	@Override
	public MpiMode getMode() {
		return MpiMode.PARALLEL;
	}

	@Override
	public String[] initialise(String[] args) {
		try {
			String[] remainingArgs = MPI.Init(new String[] {}); // ompi mpirun does not provide args via command line
			rank = MPI.COMM_WORLD.getRank();
			size = MPI.COMM_WORLD.getSize();
			return remainingArgs;
		} catch (MPIException e) {
			throw Logging.logFatalException(LOGGER, OPEN_MPI_ERROR + e.getMessage());
		}
	}

	@Override
	public int getRank() {
		return rank;
	}

	@Override
	public int getSize() {
		return size;
	}

	@Override
	public byte[] broadcastBytes(byte[] data, int source) {
		int dataLength = (rank == source) ? data.length : 0;
		int length = broadcastInt(dataLength, source);
		if (rank != source) {
			data = new byte[length];
		}
		try {
			MPI.COMM_WORLD.bcast(data, length, MPI.BYTE, source);
		} catch (MPIException e) {
			throw new RuntimeException(OPEN_MPI_ERROR + e.getMessage());
		}
		return data;
	}

	/** Copies an {@link Integer} from source to all processes
	 * 
	 * @return On all processes: the copied int */
	private int broadcastInt(int data, int source) {
		int[] buffer = new int[1];
		if (rank == source) {
			buffer[0] = data;
		}
		try {
			MPI.COMM_WORLD.bcast(buffer, 1, MPI.INT, source);
		} catch (MPIException e) {
			throw new RuntimeException(OPEN_MPI_ERROR + e.getMessage());
		}
		return buffer[0];
	}

	@Override
	public void sendBytesTo(byte[] data, int target, int tag) {
		try {
			MPI.COMM_WORLD.send(data, data.length, MPI.BYTE, target, tag);
		} catch (MPIException e) {
			throw new RuntimeException(OPEN_MPI_ERROR + e.getMessage());
		}
	}

	@Override
	public byte[] receiveBytesWithTag(int tag) {
		byte[] bytes;
		try {
			Status mpiStatus = MPI.COMM_WORLD.probe(MPI.ANY_SOURCE, tag);
			int length = mpiStatus.getCount(MPI.BYTE);
			int source = mpiStatus.getSource();
			bytes = new byte[length];
			MPI.COMM_WORLD.recv(bytes, length, MPI.BYTE, source, tag);
		} catch (MPIException e) {
			throw new RuntimeException(OPEN_MPI_ERROR + e.getMessage());
		}
		return bytes;
	}

	@Override
	public OmpiRequest iSendBytesTo(byte[] data, int target, int tag) {
		ByteBuffer buffer = MPI.newByteBuffer(data.length);
		buffer.put(data);
		try {
			return new OmpiRequest(MPI.COMM_WORLD.iSend(buffer, data.length, MPI.BYTE, target, tag));
		} catch (MPIException e) {
			throw new RuntimeException(OPEN_MPI_ERROR + e.getMessage());
		}
	}

	@Override
	public void invokeFinalize() {
		try {
			MPI.Finalize();
		} catch (MPIException e) {
			throw Logging.logFatalException(LOGGER, OPEN_MPI_ERROR + e.getMessage());
		}
	}

}
