package de.dlr.gitlab.fame.mpi;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import de.dlr.gitlab.fame.logging.Logging;
import mpi.MPIException;
import mpi.Request;

/** Facade to OpenMPI Requests
 * 
 * @author Christoph Schimeczek */
public class OmpiRequest implements MpiRequestFacade {
	static final String WAIT_EXCEPTION = "Wait for request completion failed due to: ";
	
	private static final Logger LOGGER = LoggerFactory.getLogger(OmpiRequest.class);
	
	private Request request;
	
	/** Creates an OpenMPI request-handler
	 * 
	 * @param request to handle */
	public OmpiRequest(Request request) {
		this.request = request;
	}

	@Override
	public void waitForCompletion() {
		try {
			request.waitFor();
		} catch (MPIException e) {
			Logging.logAndThrowFatal(LOGGER, WAIT_EXCEPTION + e.getMessage());
		}
	}
}
