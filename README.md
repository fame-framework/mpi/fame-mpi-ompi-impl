# FAME Mpi OpenMPI

Concrete MPI API implementation using OpenMPI for fame parallelisation.

## Available Support
This is a purely scientific project, hence there will be no paid technical support.
Limited support is available, e.g. to enhance FAME, fix bugs, etc.
To report bugs or pose enhancement requests, please file issues following the provided templates (see also [CONTRIBUTING.md](CONTRIBUTING.md))
For substantial enhancements, we recommend that you contact us via [fame@dlr.de](mailto:fame@dlr.de) for working together on the code in common projects or towards common publications and thus further develop FAME.

## Local Development

This repository uses the `openmpi-4.1.1.jar` which is not available in the central maven repository.
It must be provided manually prior to any build.

### Eclipse
You can run the `InstallMpj.launch` configuration, which is a wrapper to the underlying maven goal.

### Maven

You can run the following mvn command to install the required file manually: `mvn install:install-file`